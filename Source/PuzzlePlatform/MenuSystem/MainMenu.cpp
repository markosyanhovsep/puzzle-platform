// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenu.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"
#include "Components/EditableTextBox.h"
#include "UObject/ConstructorHelpers.h"

#include "SessionInfo.h"

UMainMenu::UMainMenu(const FObjectInitializer& ObjectInitializer):UUserWidget(ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> ServerInfoBPClass(TEXT("/Game/MenuSystem/WBP_SessionInfo"));
	if (!ensure(ServerInfoBPClass.Class != nullptr)) return;
	ServerInfoClass = ServerInfoBPClass.Class;
}

bool UMainMenu::Initialize()
{
	Super::Initialize();
	if (!HostButton) return false;
	HostButton->OnClicked.AddDynamic(this, &UMainMenu::OpenHostMenu);
	if (!JoinButton) return false;
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);
	if (!CancelButton) return false;
	CancelButton->OnClicked.AddDynamic(this, &UMainMenu::ReturnMainMenu);
	if (!JoinGameButton) return false;
	JoinGameButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);
	if (!JoinGameButton) return false;
	QuitButton->OnClicked.AddDynamic(this, &UMainMenu::QuitGame);
	if (!CancelHostButton) return false;
	CancelHostButton->OnClicked.AddDynamic(this, &UMainMenu::ReturnMainMenu);
	if (!HostGameButton) return false;
	HostGameButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);
	return true;
}

void UMainMenu::OpenHostMenu()
{
	if (HostMenu)
	{
		MenuSwitcher->SetActiveWidget(HostMenu);
	}
}

void UMainMenu::HostServer()
{
	UE_LOG(LogTemp, Warning, TEXT("I'm hosting server"))
	if (MenuInterface)
	{
		FString SessionName = SessionNameBox->GetText().ToString();
		MenuInterface->Host(SessionName);
	}
}

void UMainMenu::SetSessionList(const TArray<FServerData>& SessionNames)
{
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;
	SessionsBox->ClearChildren();
	uint32 index = 0;
	for (const auto& SessionData : SessionNames)
	{
		auto SessionWidget = CreateWidget<USessionInfo>(World, ServerInfoClass);
		SessionWidget->SessionName->SetText(FText::FromString(SessionData.Name));
		SessionWidget->HostUser->SetText(FText::FromString(SessionData.HostUsername));
		FString FractionText = FString::Printf(TEXT("%d/%d"), SessionData.CurrentPlayers, SessionData.MaxPlayers);
		SessionWidget->ConnectionFraction->SetText(FText::FromString(FractionText));
		SessionWidget->Setup(this, index++);
		if (!ensure(SessionWidget != nullptr)) return;
		SessionsBox->AddChild(SessionWidget);
	}
}

void UMainMenu::SelectIndex(uint32 index)
{
	SelectedIndex = index;
	UpdateChildren();
}

void UMainMenu::UpdateChildren()
{
	for (int32 i = 0; i < SessionsBox->GetChildrenCount(); ++i)
	{
		auto Row = Cast<USessionInfo>(SessionsBox->GetChildAt(i));
		if (Row != nullptr)
		{
			Row->Selected = (SelectedIndex.IsSet() && SelectedIndex.GetValue() == i);
		}
	}
}

void UMainMenu::JoinServer()
{
	if (SelectedIndex.IsSet() && MenuInterface)
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected %d"), SelectedIndex.GetValue());
		MenuInterface->Join(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Not Selected Server"));
	}
}

void UMainMenu::OpenJoinMenu()
{
	if (JoinMenu)
	{
		MenuSwitcher->SetActiveWidget(JoinMenu);
		if (MenuInterface)
		{
			MenuInterface->RequestSessionList();
		}
	}
}

void UMainMenu::ReturnMainMenu()
{
	if (MainMenu)
	{
		MenuSwitcher->SetActiveWidget(MainMenu);
	}
}

void UMainMenu::QuitGame()
{
	UWorld* World = GetWorld();
	if (!World) return;
	APlayerController* PC = World->GetFirstPlayerController();
	if (!PC) return;
	PC->ConsoleCommand("quit");
}

void UMainMenu::SetMenuInterface(IMenuInterface* MenuInterface)
{
	this->MenuInterface = MenuInterface;
}

void UMainMenu::Setup()
{
	this->AddToViewport();
	UWorld* World = GetWorld();
	if (!World) return;
	APlayerController* PC = World->GetFirstPlayerController();
	if (!PC) return;
	FInputModeUIOnly InputMode;
	InputMode.SetWidgetToFocus(this->TakeWidget());
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	PC->SetInputMode(InputMode);
	PC->bShowMouseCursor = true;
}

void UMainMenu::Teardown()
{
	this->RemoveFromViewport();
	UWorld* World = GetWorld();
	if (!World) return;
	APlayerController* PC = World->GetFirstPlayerController();
	if (!PC) return;
	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(true);
	PC->SetInputMode(InputMode);
	PC->bShowMouseCursor = false;
}