// Fill out your copyright notice in the Description page of Project Settings.


#include "SessionInfo.h"
#include "Components/Button.h"

#include "MainMenu.h"

void USessionInfo::Setup(class UMainMenu* Parent, uint32 index)
{
	SessionButton->OnClicked.AddDynamic(this, &USessionInfo::OnClick);
	this->Parent = Parent;
	Index = index;

}

void USessionInfo::OnClick()
{
	UE_LOG(LogTemp, Warning, TEXT("Clicked %d"), Index);
	Parent->SelectIndex(Index);
}