// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SessionInfo.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORM_API USessionInfo : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SessionName;

	UPROPERTY(meta = (BindWidget))
	class UButton* SessionButton;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* HostUser;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ConnectionFraction;

	UPROPERTY(BlueprintReadOnly)
	bool Selected;

	void Setup(class UMainMenu* Parent, uint32 index);

private:

	UFUNCTION()
	void OnClick();

	class UMainMenu* Parent;

	uint32 Index;
	
};
