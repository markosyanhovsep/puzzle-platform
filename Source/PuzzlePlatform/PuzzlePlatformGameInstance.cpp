// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzlePlatformGameInstance.h"

#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "OnlineSessionSettings.h"

#include "MenuSystem/MainMenu.h"
#include "PlatformTrigger.h"

const static FName SESSION_NAME = TEXT("Game");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("HostServerName");

UPuzzlePlatformGameInstance::UPuzzlePlatformGameInstance(const FObjectInitializer& ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/MenuSystem/WBP_MainMenu"));
	if (!ensure(MenuBPClass.Class != nullptr)) return;

	MenuClass = MenuBPClass.Class;
}

void UPuzzlePlatformGameInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Found class %s"), *MenuClass->GetName());
	IOnlineSubsystem* OnlineSubsystem = IOnlineSubsystem::Get();
	if (OnlineSubsystem)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found Online Subsystem: %s"), *OnlineSubsystem->GetSubsystemName().ToString())
		SessionInterface = OnlineSubsystem->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, 
				&UPuzzlePlatformGameInstance::OnCreateSessionCompleted);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this,
				&UPuzzlePlatformGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this,
				&UPuzzlePlatformGameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this,
				&UPuzzlePlatformGameInstance::OnJoinSessionsComplete);
		}
	}
}

void UPuzzlePlatformGameInstance::Host(const FString& ServerName)
{
	SessionSettingName = ServerName;
	if (SessionInterface.IsValid())
	{
		auto SessionName = SessionInterface->GetNamedSession(SESSION_NAME);
		if (!SessionName)
		{
			CreateSession();
		}
		else
		{
			SessionInterface->DestroySession(SESSION_NAME);
		}
	}
}

void UPuzzlePlatformGameInstance::CreateSession()
{
	if (SessionInterface.IsValid())
	{
		FOnlineSessionSettings SessionSettings;
		if (IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		else
		{
			SessionSettings.bIsLANMatch = false;
		}
		SessionSettings.NumPublicConnections = 2;
		SessionSettings.bShouldAdvertise = true;
		//SessionSettings.bUsesPresence = true;
		SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, SessionSettingName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);
		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void UPuzzlePlatformGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
	if (bWasSuccessful)
	{
		UE_LOG(LogTemp, Warning, TEXT("Session Destroyed"));
		CreateSession();
	}
}

void UPuzzlePlatformGameInstance::OnCreateSessionCompleted(FName SessionName, bool bSuccess)
{
	if (!bSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not create session"));
		return;
	}
	if (Menu)
	{
		Menu->Teardown();
	}
	UEngine* GEngine = GetEngine();
	GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Green, "Hosting");
	UWorld* World = GetWorld();
	World->ServerTravel("/Game/Maps/Lobby?listen");
}

void UPuzzlePlatformGameInstance::OnFindSessionsComplete(bool bWasSuccessful)
{
	if (bWasSuccessful)
	{
		auto Results = SearchSettings->SearchResults;
		TArray<FServerData> SessionNames;
		for (auto& Res : Results)
		{
			FServerData Data;
			Data.CurrentPlayers = Res.Session.NumOpenPublicConnections;
			Data.MaxPlayers = Res.Session.SessionSettings.NumPublicConnections;
			Data.CurrentPlayers = Data.MaxPlayers - Res.Session.NumOpenPublicConnections;
			Data.HostUsername = Res.Session.OwningUserName;
			FString ServerName;
			if (Res.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
			{
				Data.Name = ServerName;
			}
			else
			{
				Data.Name = "Could not find name.";
			}
			SessionNames.Add(Data);
		}
		if(Menu)
		{
			Menu->SetSessionList(SessionNames);
		}
	}
}

void UPuzzlePlatformGameInstance::RequestSessionList()
{
	SearchSettings = MakeShareable(new FOnlineSessionSearch());
	//SearchSettings->MaxSearchResults = 100;
	//SearchSettings->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	UE_LOG(LogTemp, Warning, TEXT("Starting Find Sessions"));
	SessionInterface->FindSessions(0, SearchSettings.ToSharedRef());
}

void UPuzzlePlatformGameInstance::Join(uint32 Index)
{
	if (!SessionInterface.IsValid()) return;
	if (Menu)
	{
		Menu->Teardown();
	}
	FOnlineSessionSearchResult SearchResult = SearchSettings->SearchResults[Index];
	SessionInterface->JoinSession(0, SESSION_NAME, SearchResult);
}

void UPuzzlePlatformGameInstance::OnJoinSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	if (Result == EOnJoinSessionCompleteResult::Type::Success)
	{
		if (SessionInterface->GetResolvedConnectString(SessionName, ConnectInfo))
		{
			UEngine* GEngine = GetEngine();
			GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Green, "Joining " + ConnectInfo);
			auto PC = GetFirstLocalPlayerController();
			PC->ClientTravel(ConnectInfo, ETravelType::TRAVEL_Absolute);
		}
	}
}

void UPuzzlePlatformGameInstance::LoadMenu()
{
	Menu = CreateWidget<UMainMenu>(this, MenuClass);
	Menu->bIsFocusable = true;
	Menu->Setup();
	Menu->SetMenuInterface(this);
}
