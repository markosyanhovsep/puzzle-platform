# PuzzlePlatform

Developed with Unreal Engine 4

I did this project following the Udemy course https://www.udemy.com/course/unrealmultiplayer/ 

### I learned how to:

* Create multiplayer games with Unreal Engine 4
* Connect game via Steam API
* Replicate the state
* Build advanced UI with UMG and C++
